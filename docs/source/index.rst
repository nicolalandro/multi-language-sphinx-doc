.. Omni-DOC documentation master file, created by
   sphinx-quickstart on Sat Apr  1 12:16:58 2023.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Omni-DOC's documentation!
====================================

.. toctree::
   :maxdepth: 3
   :caption: Contents:

   readme_link
   python_code/modules
   jsdoc
   terraform_link
