import os
import sys
sys.path.insert(0, os.path.abspath('../..'))

project = 'Omni-DOC'
copyright = '2023, Nicola Landro'
author = 'Nicola Landro'
release = '0.1'

extensions = [
    'sphinx.ext.duration',
    'sphinx.ext.doctest',
    'sphinx.ext.autodoc',
    'myst_parser',
    'sphinx_js',
]

js_source_path = '../../src_js'

templates_path = ['_templates']
exclude_patterns = []

html_theme = 'sphinx_rtd_theme'
html_static_path = ['_static']
