"""
This mpodule is an example for play
"""

def fn(a, b):
    """
    fn function

    :param a: numeric value that is the base for the operation.
    :type a: numeric or None
    :param b: numeric value that is the exponential value of the operation.
    :type b: numeric or None
    :return: The result of a ** b.
    :rtype: numeric

    """

    return a ** b

class Obj:
    """
    Class Obj

    :class:`app.Obj` class

    Example::

        c = Obj()
        c.fn1()
    """
    
    def fn1():
        """
        This is a simple function that I do not document a lot.
        """
        return 'hello'
