terraform {
}

provider "kubernetes" {
  config_path = "~/.kube/k3s"
}

resource "kubernetes_deployment" "vscode_deployment" {
  metadata {
    name = "vscode"
    labels = {
      component = "vscode-label"
    }
  }
  spec {
    replicas = 1

    selector {
      match_labels = {
        component = "vscode-label"
      }
    }

    template {
      metadata {
        labels = {
          component = "vscode-label"
        }
      }

      spec {
        container {
          image = "registry.gitlab.com/nicolalandro/vscode_docker:fedora"
          name  = "vscode"

          resources {
            limits = {
              cpu    = "0.25"
              memory = "512Mi"
            }
            requests = {
              cpu    = "0.25"
              memory = "512Mi"
            }
          }

          #   liveness_probe {
          #     http_get {
          #       path = "/"
          #       port = 15672
          #     }
          #     initial_delay_seconds = 2
          #     period_seconds        = 1
          #   }

          port {
            container_port = 8080
          }
        }
      }

    }
  }
}

resource "kubernetes_service" "code_service" {
  metadata {
    name = "vscode-web"
  }
  spec {
    selector = {
      component = "vscode-label"
    }
    port {
      port        = 8080
      target_port = 8080
      node_port   = 31000
    }

    type = "LoadBalancer"
  }
}
