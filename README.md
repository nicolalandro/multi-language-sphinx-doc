[![pipeline status](https://gitlab.com/nicolalandro/multi-language-sphinx-doc/badges/main/pipeline.svg)](https://gitlab.com/nicolalandro/multi-language-sphinx-doc/-/commits/main)
[![gitlab pages deployed page link](https://img.shields.io/badge/Pages-Doc-orange?logo=gitlab)](https://nicolalandro.gitlab.io/multi-language-sphinx-doc/)

## DEMO Documentation multi language
This codebase is a template to generate multi language documentation with sphinx. it support:
* Markdown
* python
* terrafomr
* js
* (reStructuredText)

### Sphinx DOC generation

```
# generate terraform doc
# sudo rm terraform/DOC-Terraform.md
docker-compose -f docs/docker-compose.yml run sphinx terraform-docs markdown table --output-file ../terraform/DOC-Terraform.md --output-mode inject ../terraform/

# generate Python doc
# sudo rm -rf docs/source/python_code
docker-compose -f docs/docker-compose.yml run sphinx sphinx-apidoc -o source/python_code ../
docker-compose -f docs/docker-compose.yml run sphinx sed -i "1s/.*/Py Doc/" source/python_code/modules.rst
# Manual Write js doc into docs/source/jsdoc.rst

# build
docker-compose -f docs/docker-compose.yml run sphinx make html

# Serve
docker-compose -f docs/docker-compose.yml up
# go to localhost:9001
```

### References
* python
* javascript/typescript
* terraform
* docker
* sphinx ([example](https://gitlab.com/nicolalandro/python-sphinx-docs/-/tree/main/)):
  * [sphinx basics](https://www.sphinx-doc.org/en/master/usage/quickstart.html): for documentation server
  * [Document your code with sphinx](https://software.belle2.org/development/sphinx/framework/doc/atend-doctools.html)
  * [sphinx rtd theme](https://sphinx-themes.org/sample-sites/sphinx-rtd-theme/): a sphinx theme
  * [video tutorial document with sphinx](https://www.youtube.com/watch?v=WcUhGT4rs5o)
  * [Add markdown support](https://cerodell.github.io/sphinx-quickstart-guide/build/html/markdown.html)
  * [Sphinx autodoc](https://www.freecodecamp.org/news/sphinx-for-django-documentation-2454e924b3bc/)
  * [Sphinx JS](https://github.com/mozilla/sphinx-js)
